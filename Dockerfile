FROM debian:buster-slim
LABEL maintainer="Mathias WOLFF <mathias@celea.org>"
ENV VERSION 1.0.0

RUN apt-get update -qq && apt-get install -y --no-install-recommends gnupg2
RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xfb40d3e6508ea4c8

RUN echo "deb     http://deb.kamailio.org/kamailio54 buster main" >> /etc/apt/sources.list
RUN echo "deb-src http://deb.kamailio.org/kamailio54 buster main" >> /etc/apt/sources.list

RUN apt-get update -qq && apt-get install -y --no-install-recommends \
kamailio \
redis-tools \
kamailio-json-modules \
kamailio-utils-modules \
kamailio-redis-modules \
kamailio-extra-modules \
kamailio-xml-modules \
kamailio-outbound-modules

VOLUME /etc/kamailio

# clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Config files
ADD ./src/kamailio.cfg /etc/kamailio/kamailio.cfg
ADD ./src/dispatcher.list /etc/kamailio/dispatcher.list
ADD ./src/dbtext /etc/kamailio/dbtext

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
CMD ["/docker-entrypoint.sh"]
